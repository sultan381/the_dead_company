import React from 'react';
import PageLayout from "../../components/PageLayout";
import styles from './aboutUs.module.scss'
import AboutUsImg from '../../assets/images/images/aboutus.png';


const AboutUs = () => {
 
    var data =[
        ['The Vision','The Dead Company is formed by a group of highly creative and futuristic minded individuals, who share the same common vision. We aim to create a brand that will ride the wave of this societal digitalisation shift, by being at the forefront of several industries simultaneously. We will be presenting our community with NFT projects that possess art work they can truly be proud of, and utilities that will benefit you both in the Metaverse and IRL.'],
        ['Fashion', 'We will be releasing our own line of limited supply clothing pieces, both independently, and in collaboration with huge names in the fashion world.'],
        ['Gaming', `We are already in advanced talks with developers regarding the creation of a FUD proof, P2E game with mind-blowing graphics. That's all we can say for now.`],
        ['Media','We will be hiring a team to start our own Podcast show, giving our community the option to vote for which notable figures we bring on, and what questions to ask. The future of podcasting lies within the interaction between guests, and listeners- and we have some very exciting ideas for this. '],
        ['Community','The success of a company and brand is measured by the happiness of its community. We will allow our TDC family to have a pivotal role in every decision we make- after all, we are here to serve you.'],

    ]

    return (
        <PageLayout title={AboutUsImg} titleDivclassName={styles.aboutusTitle} aboutdata={data}>
        </PageLayout>
    )
}

export default AboutUs;