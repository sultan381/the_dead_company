import React from "react";
import PageLayout from "../../components/PageLayout";
import comingSoon from "../../assets/images/images/comingsoon.png";
import styles from "./soon.module.scss";

function CommingSoon() {
  return (
    <PageLayout
      backgroundColor="#000"
      banner={comingSoon}
      className={styles.banner}
      marginTop="0px"
    ></PageLayout>
  );
}

export default CommingSoon;
