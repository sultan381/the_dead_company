import React from 'react';
import deadCompany from '../../assets/dead.svg';
import homeVideo from '../../assets/videos/TDCHomepage-1.mov'
import styles from '../home/home.module.scss';
import { Col, Container } from 'react-bootstrap';
import AppHeader from '../../components/Header';
import AppFooter from '../../components/Footer';
import PageLayout from '../../components/PageLayout';

const HomeScreen = () => {
    return (
        <PageLayout BGclassName={styles.deadcompanyBox}   videourl={homeVideo}/>
    )
}

export default HomeScreen;