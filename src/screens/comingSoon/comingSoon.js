import React from 'react';
import Header from '../../components/navbar-header/navbar-header';
import ComingSoon from '../../assets/coming-soon.svg';
import Footer from '../../components/navbar-footer/navbar-footer';
import styles from '../comingSoon/comingSoon.module.scss';
import { Col, Container } from 'react-bootstrap';

const ComingSoonScreen = () => {
    return (
        <Container fluid>
            <div className={styles.ComingSoonContainer}>
                <Col md={12}>
                    <Header />
                </Col>
                <Col md={12}>
                    <img className={styles.ComingSoonImage} src={ComingSoon}></img>
                </Col>
                <Col md={12}>
                    <Footer />
                </Col>
            </div>
        </Container>
    )
}

export default ComingSoonScreen;