import React from "react";
import styles from "./roadmap.module.scss";
import lightning from "../../assets/images/icons/YellowLightning10.png";

const data = [
  {
    title: 'Phase 1',
    desc1: 'Launch The Dead Collection'
  },
  {
    title: 'Phase 2',
    desc1: 'Start development of Marketplace',
    desc2: 'Billboard Ads',
    desc3: ' Celebrity endorsement'
  },
  {
    title: 'Phase 3',
    desc1: 'Launch Marketplace',
    desc2: 'First Merch drop'
  },
  {
    title: 'Phase 4',
    heading: 'Utility rewards:',
    desc1: 'First payment distribution to our holders',
    desc2: '66 random holders will receive a limited edition ‘the dead company’ cap, with their unique NFT serial number',
    desc3: '6 random holders will receive a real life figure of their NFT'
  },
  {
    title: 'Phase 5',
    desc1: 'Grand reveal of what NFTs give you an automatic WL spot for our next project.',
    desc2: 'Invitations to holders to an exclusive private party in London with influencers and famous music artists.'
  },
  {
    title: 'Phase 6',
    desc1: 'TBA'
  },
]

function RoadMap() {
  return (
    <div className={styles.roadmap}>
      <p className={styles.heading}>ROADMAP</p>
      {data.map((data) => (
        <div className={styles.content}>
          <p className={styles.title}>{data.title}</p>
          <p className={styles.head}>{data.heading}</p>
          <p className={styles.desc}>{data.desc1}</p>
          <p className={styles.desc}>{data.desc2}</p>
          <p className={styles.desc}>{data.desc3}</p>
          <div className={styles.icon_div}>
            <img src={lightning} />
          </div>
        </div>
      ))}
    </div>
  );
}

export default RoadMap;
