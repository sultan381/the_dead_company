import React, {useState} from "react";
import {
  Navbar,
  Container,
  Nav,
  NavbarBrand,
  NavDropdown,
} from "react-bootstrap";
import styles from "./header.module.scss";
import TDCLogo from "../../assets/images/images/TDCLogo.png";
import insta from "../../assets/images/icons/insta.svg";
import discord from "../../assets/images/icons/discord.svg";
import twitter from "../../assets/images/icons/twitter.svg";
import skull from "../../assets/images/icons/YellowSkull.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faX, } from "@fortawesome/free-solid-svg-icons";
import { FaDiscord, FaTwitter } from 'react-icons/fa';
import { AiFillInstagram } from 'react-icons/ai';

const AppHeader = () => {
  const [dropdownIcon, setDropdownIcon] = useState(faBars)
    
  return (
    <div className="app-header">
      <Navbar collapseOnSelect expand="lg">
        <div className="logo-div">
          <img src={skull} />
        </div>
        <div className="social-icons">
        <a href="http://instagram.com/the_deadcompany">
          <img src={insta} style={{marginTop:3}}/>
        </a>
        <a href="https://discord.gg/nB4mStgbH5">
          <img src={discord}  height="30px" style={{marginTop:6}} />
        </a>
        <a href="http://twitter.com/the_deadcompany">
          <img src={twitter}  height="30px" style={{marginTop:6}} />
        </a>
        </div>
        {/* <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand> */}
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <div className="img-div">
            <img src={TDCLogo} />
          </div>
          <Nav className="sm-menu">
            <Nav.Link href="/">HOME</Nav.Link>
            <Nav.Link href="/thedeadcompany">THE DEAD COLLECTION</Nav.Link>
            <Nav.Link href="/marketplace">MARKETPLACE</Nav.Link>
            <Nav.Link href="/shop">SHOP</Nav.Link>
            <Nav.Link href="/thedeadcompany#team">TEAM</Nav.Link>
            <Nav.Link href="/aboutus">ABOUT US</Nav.Link>
            <a className={styles.nftflexButton} href="https://nftflex.store"><button>NFTFLEX</button></a>
            <div className="nav-social-icons">
            <a href="http://instagram.com/the_deadcompany">
        <AiFillInstagram />
          {/* <img src={insta} style={{marginTop:3}}/> */}
        </a>
        <a href="https://discord.gg/nB4mStgbH5">
          {/* <img src={discord}  height="30px" style={{marginTop:6}} /> */}
            <FaDiscord />
        </a>
        <a href="http://twitter.com/the_deadcompany">
        <FaTwitter />
          {/* <img src={twitter}  height="30px" style={{marginTop:6}} /> */}
        </a>
            </div>
          </Nav>
        </Navbar.Collapse>
        <Nav className="menu-dropdown">
          <Nav.Link className="header-bttn" href="#deets">
            Connect
          </Nav.Link>
          <NavDropdown onToggle={(event) => event == false ? setDropdownIcon(faBars): setDropdownIcon(faX) } title={
            <FontAwesomeIcon icon={dropdownIcon} />  
    }
     id="collasible-nav-dropdown">
            <NavDropdown.Item href="/">HOME</NavDropdown.Item>
            <NavDropdown.Item href="/thedeadcompany">
              THE DEAD COLLECTION
            </NavDropdown.Item>
            <NavDropdown.Item href="/marketplace">MARKETPLACE</NavDropdown.Item>
            <NavDropdown.Item href="/shop">SHOP</NavDropdown.Item>
            <NavDropdown.Item href="/thedeadcompany#team">TEAM</NavDropdown.Item>
            <NavDropdown.Item href="/aboutus">ABOUT US</NavDropdown.Item>
            <a className={styles.nftflexButton} href="https://nftflex.store"><button>NFTFLEX</button></a>
          </NavDropdown>
        </Nav>
      </Navbar>
    </div>
  );
};

export default AppHeader;
{
  /* <Navbar.Toggle /> */
}
