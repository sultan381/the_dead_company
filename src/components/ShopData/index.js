import React from "react";
import styles from "./shopData.module.scss";
import {Row,Col, Container } from 'react-bootstrap'
import shopImage1 from '../../assets/images/shop/BH_TDC.png'
import shopImage2 from '../../assets/images/shop/BLACKH_TDC.png'
import shopImage3 from '../../assets/images/shop/PUFFER_TDC.png'
import shopImage4 from '../../assets/images/shop/HODDIE_TDC.png'


export const ShopData = () => {
  return (
    <div className={styles.shopDataSection}>
        <h3>A visionary fashion line on the quest of pushing artistic boundaries.</h3>
        <div className={styles.shopItems}>
           <div className={styles.item}>
                <img src={shopImage1}/>
            </div>
           <div className={styles.item}>
                <img src={shopImage2}/>
            </div>
           <div className={styles.item}>
                <img src={shopImage3}/>
            </div>
           <div className={styles.item}>
                <img src={shopImage4}/>
            </div>
      
        </div>
        
    </div>
  );
};
