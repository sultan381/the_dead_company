import React from "react";
import styles from "./style.module.scss";
import artGalleryImage1 from '../../assets/images/marketplace/artgallery1.png'
import artGalleryImage2 from '../../assets/images/marketplace/artgallery2.png'

export const MarketPlaceData = () => {
  return (
    <div className={styles.marketplaceSection}>
      <h3>Welcome to the future of the NFT marketplace</h3>

      <div className={styles.mode}>
        <h3 className={styles.heading}>Classic Mode</h3>
        <p>
          Users will have the option to opt for Classic Mode, a simple and easy
          to use layout when browsing through NFT collections.
        </p>
      </div>

      <div className={styles.mode}>
        <h3 className={styles.heading}>Gallery Mode</h3>
        <p>
          This mode enables users to browse through collections via a fully
          immersive 3D art gallery experience. Creators are able to host private
          exhibitions and choose between hundreds of galleries to showcase their
          art. 
        </p>
        <div className={styles.marketView}>
          <div className={styles.item}>
            <img src={artGalleryImage1} />
          </div>
          <div className={styles.item}>
            <img src={artGalleryImage2} />
          </div>
        </div>
      </div>
    </div>
  );
};
