import React from "react";
import { Col, Row, Accordion } from "react-bootstrap";
import styles from "./faqs.module.scss";
import deadBanner from "../../assets/images/images/samurai.JPG";

const data = [
  {
    key: 0,
    title: "What is the Total Supply of The Dead Collection?",
    desc: "There are a total of 3,333 NFTs available for mint.",
  },
  {
    key: 1,
    title: "How do I know if Im on the Whitelist?",
    desc: "Make sure to read the announcements channel on our Discord frequently so you don't accidentally miss out.    ",
  },
  {
    key: 2,
    title: "What day does The Dead Collection Launch?",
    desc: "TBA in our Discord.",
  },
  {
    key: 3,
    title: "How do I join the whitelist?",
    desc: "Head over to the whitelist channel in our Discord and follow the instructions there.",
  },
  {
    key: 4,
    title: "How many Whitelist spots are available?",
    desc: "1,500",
  },
  {
    key: 5,
    title: "How many NFTs can I mint in the Whitelist Sale?",
    desc: "2 per whitelisted address.",
  },
  {
    key: 6,
    title: "How many NFTs can I mint in the Public Sale?",
    desc: "Up to 2 per wallet.",
  },
  {
    key: 7,
    title: "What will the mint price be?",
    desc: "TBA in our Discord.",
  },
  {
    key: 8,
    title: "Network?",
    desc: "Ethereum",
  },
];

function FAQs() {
  return (
    <div className={styles.faq}>
      <p className={styles.title}>FAQ'S</p>
      <Row className={styles.row}>
        <Col lg={5}  className='d-md-none d-sm-none d-lg-block'>
          <div className={styles.banner_div}>
            <img src={deadBanner} />
          </div>
        </Col>
        <Col sm={12} lg={7}>
          <div className="faq-content">
            <Accordion defaultActiveKey="0">
              {data.map((data) => (
                <Accordion.Item eventKey={data.key}>
                  <Accordion.Header>{data.title}</Accordion.Header>
                  <Accordion.Body>{data.desc}</Accordion.Body>
                </Accordion.Item>
              ))}
            </Accordion>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default FAQs;
