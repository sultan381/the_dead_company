import React from "react";
import { Row, Col } from "react-bootstrap";
import styles from "./team.module.scss";
import spink from "../../assets/images/icons/spink.png";
import sblue from "../../assets/images/icons/sblue.png";
// import sgreen from "../../assets/images/icons/sgreen.png";
import sgreen from "../../assets/images/icons/sgreen.png";
import skull from "../../assets/images/icons/skull.png";
import sred from "../../assets/images/icons/sred.png";
import smid from "../../assets/images/icons/smid.png";

const data = [
  {
    img: spink,
    title: "Jacob - Founder",
    desc: "Jacob is a serial entrepreneur who’s had success with multiple start ups both as an owner and an investor. His most recent venture, saw the birth of the worlds first NFT merchandise customisation platform- nftflex.store. His vision with The Dead Company, is to create the ‘Supreme’ of the tech world. By integrating blockchain technology, fashion, gaming, and pop culture, he is confident that TDC will be solidified as a blue chip brand, worthy of admiration for years to come.",
  },
  {
    img: sblue,
    title: "Aaron - Creative Director ",
    desc: "Aaron spent 3 years as an intern at Zara and Adobe, where he inspired, designed and executed campaigns reaching millions. Aaron began to pay attention to the digital shift of artistry as a result of the recent explosion in the NFT space, leaving him with no choice but to be part of a community where limitless creativity thrives.",
  },
  {
    img: sgreen,
    title: "Max - 3D Graphic Artist ",
    desc: "Max brings in a wealth of experience in various creative fields. He has spent the last 7+ years in:",
    desc1:
      "His passion for digital art, combined with an unparalleled creative versatility, is a breath of fresh air to this rapidly evolving NFT space.",
    points: [
      "- 3D modelling & composing",
      "- Motion graphics design",
      "- TV & Social Media advertising",
      "- Conceptual design",
      "- Art directing & Photography",
    ],
  },
  {
    img: skull,
    title: "Jay - Chief Marketing Officer ",
    desc: "Jay is a Mathematics graduate from the UK with extensive experience in Sales and Marketing. Having worked across a variety of industries including Insurance, Financial Services and most recently in Tech (specialising in SaaS), Jay specialises in Business Development and Digital Marketing. Entering the cryptocurrency scene back in 2017 and with a particular passion for education on the value of decentralised technologies, Jay will be supporting The Dead Company in bringing a range of real world applications for their NFT collections.",
  },
  {
    img: smid,
    title: "Harry - Lead Developer",
    desc: "Harry had successfully helped multiple sellout launches on Solana. He has worked for 7 years, running a web2 consultancy and has managed web3 teams to develop unique tech which is helping to revolutionise the NFT space. His most recent project, PlanetZ, sold out in under 90s.",
  },
  {
    img: sred,
    title: "Tom - Web3 Developer",
    desc: "Tom is a web3 developer that has been a key part in multiple teams developing staking, terraforming and breeding technology on Solana. Additionally, he has helped 10+ projects with art generation and a sellout, error-free mint.",
  },
];

function Team() {
  return (
    <div className={styles.team} id="team">
      <p className={styles.heading}>MEET THE TEAM</p>
      <Row className={styles.row}>
        {data.map((data) => (
          <Col xs={12} md={6} lg={5}>
            <div className={styles.content}>
              <div className={styles.header}>
                <div className={styles.img_div}>
                  <img src={data.img} />
                </div>
                <p className={styles.title}>{data.title}</p>
              </div>
              <p className={styles.desc}>{data.desc}</p>
              {data.points &&
                data.points.map((item) => (
                  <p className={styles.desc}>{item}</p>
                ))}
              <p className={styles.desc}>{data.desc1}</p>
            </div>
          </Col>
        ))}
      </Row>
    </div>
  );
}

export default Team;
