import React from "react";
import { Navbar, Container, Row } from "react-bootstrap";
import styles from "./footer.module.scss";
import skull from "../../assets/images/icons/YellowSkull.png";
import lightning from "../../assets/images/icons/YellowLightning10.png";


const AppFooter = () => {
  return (
    <div className={styles.row}>
      <div className={styles.icon_div}>
        <img src={skull} />
      </div>
      <div className={styles.icon_div}>
        <img className={styles.lighImage} src={lightning}  />
      </div>
      <div className={styles.icon_right_div}>
      </div>
    </div>
  );
};

export default AppFooter;
{
  /* <Navbar.Toggle /> */
}
